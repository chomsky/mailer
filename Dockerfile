# fetch the vendor with the builder platform to avoid qemu issues
FROM --platform=$BUILDPLATFORM rust:1-alpine AS vendor

ENV USER=root

WORKDIR /code
RUN cargo init
COPY .cargo/config.toml /code/.cargo/config.toml
COPY Cargo.toml /code/Cargo.toml
COPY Cargo.lock /code/Cargo.lock

# https://docs.docker.com/engine/reference/builder/#run---mounttypecache
RUN --mount=type=cache,target=$CARGO_HOME/git,sharing=locked \
  --mount=type=cache,target=$CARGO_HOME/registry,sharing=locked \
  mkdir -p /code/.cargo \
  && cargo vendor >> /code/.cargo/config.toml

FROM rust:1-alpine AS base

RUN apk add --no-cache musl-dev

ENV USER=root

WORKDIR /code

COPY Cargo.toml /code/Cargo.toml
COPY Cargo.lock /code/Cargo.lock
# COPY lib /code/lib
COPY src /code/src
COPY --from=vendor /code/.cargo /code/.cargo
COPY --from=vendor /code/vendor /code/vendor

COPY src /code/src

CMD [ "cargo", "test", "--offline" ]

FROM base AS builder

RUN cargo build --release --offline

FROM scratch AS binary

COPY --from=builder /code/target/release/mailer /mailer

FROM alpine:3

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.docker.cmd="docker run -d -p 8080:8080 -e TEMPLATE__TYPE=LOCAL -e TEMPLATE__PATH=/templates -e SMTP_SERVER=smtp.ethereal.email -e SMTP_PORT=587 -e SMTP_USERNAME=prince.marquardt93@ethereal.email -e SMTP_PASSWORD=GWGvvDCze3VDNegg88 -e FROM_EMAIL=prince.marquardt93@ethereal.email -e SMTP_CONNECTION_TYPE=starttls jdrouet/mailer"
LABEL org.label-schema.vcs-url="https://jolimail.io"
LABEL org.label-schema.url="https://github.com/jdrouet/mailer"
LABEL org.label-schema.description="Service to convert mrml to html and send it by email"
LABEL maintaner="Shanur Rahman shanur.cse.nitap@gmail.com"

RUN apk add --no-cache curl

ENV HOST=0.0.0.0
ENV PORT=8080
ENV RUST_LOG=info
ENV TEMPLATE_ROOT=/templates
ENV DISABLE_LOG_COLOR=true

COPY --from=builder /code/target/release/mailer /app/mailer
COPY ./templates /app/templates
COPY ./assets /app/assets
WORKDIR /app
EXPOSE 8080

# Add healthcheck later
# HEALTHCHECK --interval=10s --timeout=3s \
#   CMD curl --fail --head http://localhost:8080/status || exit 1

CMD [ "./mailer" ]