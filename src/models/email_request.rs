use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct EmailRequest {
    pub to: String,
    pub subject: String,
    pub body: String,
    pub template_name: String,
    pub variables: HashMap<String, String>,
    #[serde(default)]
    pub attachments: Vec<String>,
}