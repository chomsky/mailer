use actix_multipart::Multipart;
use actix_web::{delete, post, web, HttpResponse};
use futures::StreamExt;
use std::path::Path;
use tokio::{self, io::AsyncWriteExt};


#[derive(serde::Deserialize)]
struct TemplateInfo {
    name: Option<String>,
}

pub fn init(cfg: &mut web::ServiceConfig) {
    cfg
        .service(add_asset)
        .service(add_template)
        .service(delete_asset)
        .service(delete_template)
        .service(web::scope("/assets")
                .service(actix_files::Files::new("/", "./assets").show_files_listing())
                .default_service(web::route().to(serve_directory_listing)),
        )
        .service(web::scope("/templates")
                .service(actix_files::Files::new("/", "./templates").show_files_listing())
                .default_service(web::route().to(serve_directory_listing)),
        );
}

pub async fn serve_directory_listing(path: web::Path<String>) -> HttpResponse {
    let directory = format!("./{}", path);
    match tokio::fs::read_dir(&directory).await {
        Ok(mut entries) => {
            let mut html = format!("<html><body><h1>Directory Listing: {}</h1><ul>", path);
            while let Some(entry) = entries.next_entry().await.unwrap_or(None) {
                let file_name = entry.file_name().into_string().unwrap_or_default();
                let file_url = format!("{}/{}", path, file_name);
                html.push_str(&format!("<li><a href=\"{}\">{}</a></li>", file_url, file_name));
            }
            html.push_str("</ul></body></html>");
            HttpResponse::Ok().content_type("text/html").body(html)
        }
        Err(_) => HttpResponse::NotFound().finish(),
    }
}

pub async fn save_file(mut payload: Multipart, file_path: std::path::PathBuf) -> Result<HttpResponse, actix_web::error::Error> {
    let mut file = tokio::fs::File::create(file_path).await?;

    while let Some(field) = payload.next().await {
        let mut field = match field {
            Ok(field) => field,
            Err(e) => return Err(actix_web::error::ErrorBadRequest(e.to_string())),
        };

        if field.name() == "file" {
            // Write the file content to the file
            while let Some(chunk) = field.next().await {
                let chunk = match chunk {
                    Ok(chunk) => chunk,
                    Err(e) => return Err(actix_web::error::ErrorBadRequest(e.to_string()))
                };

                let _ = file.write_all(&chunk).await?;
            }
        }
    }

    Ok(HttpResponse::Ok().body("File saved successfully"))
}

#[post("/templates")]
pub async fn add_template(payload: Multipart, info: web::Query<TemplateInfo>) -> Result<HttpResponse, actix_web::error::Error> {
    // Assuming you want to save the file with a name provided by the TemplateInfo
    let file_path = std::path::PathBuf::from(format!("./templates/{}", info.name.clone().unwrap_or_else(|| "unnamed".to_string())));
    
    save_file(payload, file_path).await
}

#[delete("/templates/{filename}")]
pub async fn delete_template(filename: web::Path<String>) -> Result<HttpResponse, actix_web::Error> {
    let filepath = Path::new("./templates").join(filename.to_string());
    if filepath.exists() {
        tokio::fs::remove_file(&filepath).await?;
        Ok(HttpResponse::Ok().body(format!("Template '{}' deleted successfully", filename)))
    } else {
        Ok(HttpResponse::NotFound().body(format!("Template '{}' not found", filename)))
    }
}

#[post("/assets")]
pub async fn add_asset(payload: Multipart, info: web::Query<TemplateInfo>) -> Result<HttpResponse, actix_web::error::Error> {
    let file_path = std::path::PathBuf::from(format!("./assets/{}", info.name.clone().unwrap_or_else(|| "unnamed".to_string())));
    save_file(payload, file_path).await
}


#[delete("/assets/{filename}")]
pub async fn delete_asset(filename: web::Path<String>) -> Result<HttpResponse, actix_web::Error> {
    let filepath = Path::new("./assets").join(filename.to_string());
    if filepath.exists() {
        tokio::fs::remove_file(&filepath).await?;
        Ok(HttpResponse::Ok().body(format!("Asset '{}' deleted successfully", filename)))
    } else {
        Ok(HttpResponse::NotFound().body(format!("Asset '{}' not found", filename)))
    }
}