use crate::models::email_request::EmailRequest;
use crate::services::email_service::EmailService;
use actix_web::{post, web, HttpResponse};

#[post("/send-email")]
async fn send_email(
    email_request: web::Json<EmailRequest>,
    email_service: web::Data<EmailService>,
) -> HttpResponse {
    match email_service.send_email(
        &email_request,
        &email_request.template_name,
        &email_request.variables,
        &email_request.attachments,
    ).await {
        Ok(_) => HttpResponse::Ok().body("Email sent successfully!"),
        Err(err) => HttpResponse::InternalServerError().body(err),
    }
}

pub fn init(cfg: &mut web::ServiceConfig) {
    cfg
        .service(send_email);
}