use dotenv::dotenv;
use lettre::transport::smtp::PoolConfig;
use tokio::io::AsyncReadExt;
use std::collections::HashMap;
use std::env;
use crate::models::email_request::EmailRequest;
use handlebars::Handlebars;
use lettre::{AsyncSmtpTransport, AsyncTransport, Message, Tokio1Executor};
use lettre::transport::smtp::authentication::Credentials;
use lettre::message::{header::ContentType, Attachment, MultiPart};
use tokio::fs::File as AsyncFile;
pub struct EmailService {
    mailer: AsyncSmtpTransport<Tokio1Executor>,
    from_email: String,
}

impl EmailService {
    pub fn new() -> Self {
        dotenv().ok();

        let smtp_server = env::var("SMTP_SERVER").expect("SMTP_SERVER must be set");
        let smtp_port = env::var("SMTP_PORT").expect("SMTP_PORT must be set").parse().expect("SMTP_PORT must be a number");
        let smtp_username = env::var("SMTP_USERNAME").expect("SMTP_USERNAME must be set");
        let smtp_password = env::var("SMTP_PASSWORD").expect("SMTP_PASSWORD must be set");
        let from_email = env::var("FROM_EMAIL").expect("FROM_EMAIL must be set");
        let smtp_connection_type = env::var("SMTP_CONNECTION_TYPE").expect("SMTP_CONNECTION_TYPE must be set");

        let creds = Credentials::new(smtp_username.clone(), smtp_password.clone());
        let pool_config = PoolConfig::default()
            .max_size(10)
            .idle_timeout(std::time::Duration::from_secs(30));

        let mailer = match smtp_connection_type.as_str() {
            "starttls" => AsyncSmtpTransport::<Tokio1Executor>::starttls_relay(&smtp_server)
                .unwrap()
                .port(smtp_port)
                .credentials(creds)
                .pool_config(pool_config)
                .build(),
            "relay" => AsyncSmtpTransport::<Tokio1Executor>::relay(&smtp_server)
                .unwrap()
                .port(smtp_port)
                .pool_config(pool_config)
                .credentials(creds)
                .build(),
            _ => panic!("Invalid SMTP connection type"),
        };

        EmailService {
            mailer,
            from_email,
        }
    }

    pub async fn send_email(
        &self,
        email_request: &EmailRequest,
        template_name: &str,
        variables: &HashMap<String, String>,
        attachments: &[String],
    ) -> Result<(), String> {
        let mut handlebars = Handlebars::new();
        handlebars
            .register_template_file(template_name, format!("templates/{}.hbs", template_name))
            .map_err(|err| format!("Failed to register template: {}", err))?;
    
        let mut context = variables.clone();
        context.insert("subject".to_string(), email_request.subject.clone());
        context.insert("to".to_string(), email_request.to.clone());
    
        let rendered_body = handlebars
            .render(template_name, &context)
            .map_err(|err| format!("Failed to render template: {}", err))?;
    
        let email = Message::builder()
            .from(self.from_email.parse().unwrap())
            .to(email_request.to.parse().unwrap())
            .subject(&email_request.subject);
    
        let mut multipart = MultiPart::alternative().singlepart(
            lettre::message::SinglePart::html(rendered_body.clone()),
        );
    
        for attachment_name in attachments {
            let attachment_path = format!("assets/{}", attachment_name);
            let mut file = AsyncFile::open(&attachment_path).await
                .map_err(|err| format!("Failed to open attachment file: {}", err))?;
            let mut buffer = Vec::new();
            file.read_to_end(&mut buffer).await
                .map_err(|err| format!("Failed to read attachment file: {}", err))?;
    
            let attachment = Attachment::new(attachment_name.clone())
                .body(buffer, ContentType::TEXT_HTML);
            multipart = multipart.singlepart(attachment);
        }
    
        let email = email.multipart(multipart).unwrap();
    
        self.mailer.send(email).await.map_err(|err| format!("Failed to send email: {:?}", err))?;
    
        Ok(())
    }
}