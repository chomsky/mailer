use actix_web::{web, App, HttpServer};
use tracing::info;
use crate::handlers::{directory_listing, email_handler};
use crate::services::email_service::EmailService;
mod handlers;
mod models;
mod services;
pub mod config;
use tokio;
use dotenv::dotenv;
// #[cfg(all(target_env = "musl", target_pointer_width = "64"))]
// #[global_allocator]
// static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

#[tokio::main(flavor = "multi_thread")]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    let email_service = web::Data::new(EmailService::new());
    config::tracing_config::init_tracing();

    for (key, value) in std::env::vars() {
        println!("{}: {}", key, value);
    }
    
    let message = "Chomsky Mail Server started on port 8080";
    let width = message.len() + 4;
    println!("{}", "=".repeat(width));
    println!("= {} =", " ".repeat(width - 4));
    println!("= {} =", message);
    println!("= {} =", " ".repeat(width - 4));
    println!("{}", "=".repeat(width));
    println!("        _");
    println!("       | |");
    println!("       | |");
    println!("      _|_|_");
    println!("     |_____|");
    println!("    _|_____|_");
    println!("   |_________|");
    println!("  _|_________|_");
    println!(" |_____________|");
    println!("       | |");
    println!("       |_|");
    HttpServer::new(move || {
        App::new()
            .app_data(email_service.clone())
            .configure(directory_listing::init)
            .configure(email_handler::init)    
        })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}