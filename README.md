# Rust Mailer

[![Docker Image Size](https://img.shields.io/docker/image-size/shanurcsenitap/rust_mailer)](https://hub.docker.com/r/shanurcsenitap/rust_mailer)
[![Docker Pulls](https://img.shields.io/docker/pulls/shanurcsenitap/rust_mailer)](https://hub.docker.com/r/shanurcsenitap/rust_mailer)
[![pipeline status](https://gitlab.com/chomsky/mailer/badges/main/pipeline.svg)](https://gitlab.com/chomsky/mailer/-/commits/main)

Rust Mailer is a high-performance, scalable, and cross-platform email sending solution built with pure Rust. It leverages the power of the Tokio runtime to handle a large volume of email sending tasks efficiently.

## Why Rust Mailer?

- **Performance**: Rust Mailer is written in Rust, a language known for its speed and efficiency. It utilizes the Tokio runtime to enable asynchronous and concurrent email sending, allowing it to handle a high throughput of emails with minimal resource usage.

- **Scalability**: With its asynchronous architecture and efficient memory management, Rust Mailer can scale seamlessly to accommodate growing email sending needs. It can handle a large number of concurrent connections and deliver emails quickly and reliably.

- **Cross-Platform Compatibility**: Rust Mailer is designed to work across multiple platforms, including Linux, macOS, and Windows. It provides a consistent and reliable email sending experience regardless of the operating system.

- **Lightweight**: The Docker image for Rust Mailer is incredibly lightweight, with a size of just [![Docker Image Size](https://img.shields.io/docker/image-size/shanurcsenitap/rust_mailer)](https://hub.docker.com/r/shanurcsenitap/rust_mailer). This makes it easy to deploy and run in various environments without consuming significant system resources.

- **Reliability**: Rust's strong type system and ownership model help prevent common programming errors, ensuring that Rust Mailer is reliable and less prone to bugs. It provides a robust foundation for building email sending applications.

- **Extensibility**: Rust Mailer is built with modularity in mind, allowing developers to extend and customize its functionality to suit their specific needs. It provides a flexible and extensible architecture for integrating with various email providers and services.

## Getting Started

To get started with Rust Mailer, you can easily pull the pre-built Docker image from Docker Hub:

```bash
docker pull shanurcsenitap/rust_mailer
```

Once you have the image, you can run it using the following command:

```bash
docker run -d shanurcsenitap/rust_mailer
```

For detailed usage instructions and configuration options, please refer to the [documentation](https://github.com/shanurcsenitap/rust_mailer/docs).


## Usage (docker)
```sh
docker run -d \
  -e SMTP_SERVER=smtp.ethereal.email \
  -e SMTP_PORT=587 \
  -e SMTP_USERNAME=prince.marquardt93@ethereal.email \
  -e SMTP_PASSWORD=GWGvvDCze3VDNegg88 \
  -e FROM_EMAIL=prince.marquardt93@ethereal.email \
  -e SMTP_CONNECTION_TYPE=starttls \
  -v /path/to/assets:/app/assets \
  -v /path/to/templates:/app/templates \
  shanurcsenitap/rust_mailer:latest
```

## In docker compose
```yaml
version: '3'
services:
  rust_mailer:
    image: shanurcsenitap/rust_mailer:latest
    environment:
      - SMTP_SERVER=smtp.ethereal.email
      - SMTP_PORT=587
      - SMTP_USERNAME=prince.marquardt93@ethereal.email
      - SMTP_PASSWORD=GWGvvDCze3VDNegg88
      - FROM_EMAIL=prince.marquardt93@ethereal.email
      - SMTP_CONNECTION_TYPE=starttls
    volumes:
      - ./assets:/app/assets
      - ./templates:/app/templates
```

Templates should be stored in a folder called templates in the root directory

## Sending emails
```sh
curl -X POST -H "Content-Type: application/json" -d '{ 
  "to": "prince.marquardt93@ethereal.email",
  "subject": "Test Email",
  "body": "This is a test email sent using the API.",
  "template_name": "email_template",
  "variables": {
    "user_name": "John Doe",
    "order_number": "123456"
  }
}' http://localhost:8080/send-email
```

## Reading templates
To find out all the templates that have been loaded
```curl http://127.0.0.1:8080/templates```

## Assets
```curl http://127.0.0.1:8080/assets```

## Multipart support
Attachment must be present in assets directory
```sh
curl -X POST -H "Content-Type: application/json" -d '{ 
  "to": "prince.marquardt93@ethereal.email",
  "subject": "Test Email",
  "body": "This is a test email sent using the API.",
  "template_name": "email_template",
  "attachments": ["cat.jpg"],
  "variables": {
    "user_name": "John Doe",
    "order_number": "123456"
  }
}' http://localhost:8080/send-email
```

## Apis for managing templates
1. Add a template:
```bash
curl -X POST -F "file=@/path/to/your/template.hbs" "http://localhost:8080/templates?name=your_template_name"
```
This command sends a POST request to the `/templates` endpoint with a multipart form data that contains a file named `template.hbs` and a query parameter `name` with the value `your_template_name`.

2. Delete a template:
```bash
curl -X DELETE "http://localhost:8080/templates/your_template_name"
```
This command sends a DELETE request to the `/templates/{filename}` endpoint with `your_template_name` as the filename to be deleted.

3. Add an asset:
```bash
curl -X POST -F "file=@/path/to/your/asset.png" "http://localhost:8080/assets?name=your_asset_name"
```
This command sends a POST request to the `/assets` endpoint with a multipart form data that contains a file named `asset.png` and a query parameter `name` with the value `your_asset_name`.

4. Delete an asset:
```bash
curl -X DELETE "http://localhost:8080/assets/your_asset_name"
```
This command sends a DELETE request to the `/assets/{filename}` endpoint with `your_asset_name` as the filename to be deleted.

Please replace `/path/to/your/template.hbs` and `/path/to/your/asset.png` with the actual paths to your template and asset files, and `your_template_name` and `your_asset_name` with the actual names you want to use for your templates and assets.


## Contributing

We welcome contributions from the community to make Rust Mailer even better. If you encounter any issues, have suggestions for improvements, or want to contribute new features, please open an issue or submit a pull request on the [GitLab repository](https://gitlab.com/chomsky/mailer/-/issues).

## License

Rust Mailer is open-source software licensed under the [GNU Affero General Public License v3.0](https://gitlab.com/chomsky/mailer/-/blob/main/LICENSE).

---

Start sending emails efficiently and reliably with Rust Mailer today! If you have any questions or need further assistance, feel free to reach out to our friendly community on [GitLab Discussions](https://github.com/shanurcsenitap/rust_mailer/discussions).