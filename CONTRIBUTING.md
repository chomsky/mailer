## Contribution Guide

Thank you for your interest in contributing to the Mailer project! We welcome and appreciate contributions from the community. By contributing to this project, you can help improve the functionality, fix bugs, add new features, and enhance the overall quality of the Mailer application.

To contribute to this project, please follow these steps:

1. Fork the repository on GitLab by clicking on the "Fork" button at the top right corner of the project page.

2. Clone your forked repository to your local machine using the following command:
   ```
   git clone https://gitlab.com/your-username/mailer.git
   ```

3. Create a new branch for your contribution:
   ```
   git checkout -b feature/your-feature-name
   ```

4. Make your desired changes, additions, or bug fixes in your local branch.

5. Ensure that your code follows the project's coding conventions and style guidelines.

6. Test your changes thoroughly to make sure they work as expected and do not introduce any new issues.

7. Commit your changes with a descriptive commit message:
   ```
   git commit -m "Add your commit message here"
   ```

8. Push your changes to your forked repository:
   ```
   git push origin feature/your-feature-name
   ```

9. Open a merge request on the original repository's GitLab page. Provide a clear and detailed description of your changes, including any relevant information or screenshots.

10. Wait for the project maintainers to review your merge request. They may provide feedback or request further changes. Be responsive to their comments and make the necessary updates.

11. Once your merge request is approved, it will be merged into the main branch of the project.

We appreciate your valuable contributions to the Mailer project. Your efforts help make this project better for everyone.

If you find this project helpful, please consider giving it a star on GitLab. Your support and recognition mean a lot to us and encourage us to continue developing and improving the project.

To star the project, simply visit the project page on GitLab:
https://gitlab.com/chomsky/mailer

Click on the "Star" button at the top right corner of the page to show your appreciation.

Thank you for your contribution and support!